USE springbootjdbcthymeleaf;

DROP TABLE IF EXISTS subscribers
CREATE TABLE subscribers (
    subscriberId INTEGER PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE KEY,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP 
) ENGINE=INNODB CHARSET=utf8;

INSERT INTO subscribers (firstName, lastName, email) VALUES ("toto", "dupont", "toto.dupont@gmail.com"), ("jeanette", "miton", "jeanette.miton@gmail.com");