package co.simplon.promo16.springbootjdbcthymeleaf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import co.simplon.promo16.springbootjdbcthymeleaf.model.Subscriber;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    @Autowired
    DataSource dataSource;

    Connection cnx;
    List<Subscriber> subscribers = new ArrayList<>();

    private static final String CREATE_SUBSCRIBER = "INSERT INTO subscribers (firstName, lastName, email) VALUES (?,?,?,?)";
    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    private static final String GET_SUBSCRIBER_BY_ID = "SELECT * FROM subscribers WHERE subscriberId = ?";
    private static final String UPDATE_SUBSCRIBER_BY_ID = "UPDATE subscribers SET firstName=?, lastName=?, email=? WHERE subscriberId=?";
    private static final String DELETE_SUBSCRIBER_BY_ID = "DELETE FROM subscribers WHERE subscriberId=?";

    @Override
    public List<Subscriber> findAll() {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Subscriber subscriber = new Subscriber(
                        result.getInt("subscriberId"),
                        result.getString("firstName"),
                        result.getString("lastName"),
                        result.getString("email"),
                        result.getTimestamp("createdAt"));
                subscribers.add(subscriber);
            }
            return subscribers;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public Subscriber findById(Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_SUBSCRIBER_BY_ID);
            ResultSet result = stmt.executeQuery();

            Subscriber subscriber = new Subscriber(
                    result.getInt("subscriberId"),
                    result.getString("firstName"),
                    result.getString("lastName"),
                    result.getString("email"),
                    result.getTimestamp("createdAt"));
            return subscriber;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseEntity<String> save(Subscriber subscriber) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(CREATE_SUBSCRIBER);
            stmt.setString(1, subscriber.getFirstName());
            stmt.setString(2, subscriber.getLastName());
            stmt.setString(3, subscriber.getEmail());
            int execute = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                subscriber.setSubscriberId(result.getInt(1));
                return new ResponseEntity<>("Subscriber successfully created.", HttpStatus.OK);
            }
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Error -> new Subscriber has not been created.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> update(@PathVariable("id") Integer id, @RequestBody Subscriber updateSubscriber) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(UPDATE_SUBSCRIBER_BY_ID);
            stmt.setString(1, updateSubscriber.getFirstName());
            stmt.setString(2, updateSubscriber.getLastName());
            stmt.setString(3, updateSubscriber.getEmail());
            stmt.setInt(4, id);
            int execute = stmt.executeUpdate();
            if (execute == 1) {
                return new ResponseEntity<>("Le subscriber a été mis à jour.", HttpStatus.OK);
            }
            cnx.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> le subscriber n'a pas pu être mis à jour.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<String> deleteById(@PathVariable("id") Integer id) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(DELETE_SUBSCRIBER_BY_ID);
            stmt.setInt(1, id);
            int execute = stmt.executeUpdate();
            if (execute == 1) {
                return new ResponseEntity<>("Le subscriber a été supprimé.", HttpStatus.OK);
            }
            cnx.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error -> le subscriber n'a pas pu être supprimé.", HttpStatus.BAD_REQUEST);
    }

}
