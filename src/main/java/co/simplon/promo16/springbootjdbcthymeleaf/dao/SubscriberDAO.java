package co.simplon.promo16.springbootjdbcthymeleaf.dao;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.springbootjdbcthymeleaf.model.Subscriber;

@Repository
public interface SubscriberDAO {
    
    public List<Subscriber> findAll(); 
    public Subscriber findById(Integer id);
    public ResponseEntity<String> save(Subscriber subscriber);
    public ResponseEntity<String> update(Integer id, Subscriber subscriber);
    public ResponseEntity<String> deleteById(Integer id);

}
