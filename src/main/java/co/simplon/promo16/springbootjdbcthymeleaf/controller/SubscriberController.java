package co.simplon.promo16.springbootjdbcthymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import co.simplon.promo16.springbootjdbcthymeleaf.dao.SubscriberDAO;
import co.simplon.promo16.springbootjdbcthymeleaf.model.Subscriber;

@Controller
@RequestMapping("/subscribers")
public class SubscriberController {

    @Autowired
    SubscriberDAO subscriberDAO;

    @GetMapping("")
    public String getAllSubscribers(Model model) {
        model.addAttribute("subscribers", subscriberDAO.findAll());
        // model.addAttribute("newSubscriber", new Subscriber());
        return "index";
    }

    @GetMapping("/{id}")
    public String getSubscriberById(Integer id) {
        return "index";
    }

    @GetMapping("/new")
    public String getAddPage(Model model) {
        Subscriber subscriber = new Subscriber();
        model.addAttribute("subscriber", subscriber);
        return "add";
    }

    @PostMapping(value = "/add")
    public String addSubscriber(@ModelAttribute("subscriber") Subscriber subscriber, Model model) {
        subscriberDAO.save(subscriber);
        return "redirect:/";
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateSubscriber(@PathVariable("id") Integer id, @RequestBody Subscriber subscriber) {
        return subscriberDAO.update(id, subscriber);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSubscriber(@PathVariable("id") Integer id) {
        return subscriberDAO.deleteById(id);
    }
}
